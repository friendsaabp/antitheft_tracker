package com.caveview.anti_theft_tracker;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.caveview.anti_theft_tracker.model.SessionManager;
import com.caveview.anti_theft_tracker.utils.BatteryInformation;
import com.caveview.anti_theft_tracker.utils.ConnectionDetector;
import com.caveview.anti_theft_tracker.utils.MyAsyncTask;
import com.caveview.anti_theft_tracker.utils.URLS;

import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    EditText editMobile;
    EditText editPassword;
    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editMobile = (EditText) findViewById(R.id.mobIn);
        editPassword = (EditText) findViewById(R.id.passIn);
      //  BatteryInformation bI = new BatteryInformation();
      //  bI.getBatteryStatus(getApplicationContext());
      //  bI.getChargingStatus(getApplicationContext());
        Button b1 = (Button) findViewById(R.id.confirmButton);
        session = new SessionManager(getApplicationContext());
        if(session.isLoggedIn()){
            Intent intent = new Intent(MainActivity.this, AndroidGPSTrackingActivity.class);
            startActivity(intent);
            finish();
        }

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmFunction();
            }
        });
    }

    public void confirmFunction() {
        final ProgressDialog dialogLoginShow = new ProgressDialog(this);
        final String mobile = editMobile.getText().toString();
        final String password = editPassword.getText().toString();

        //Write Code for getting Result from server and saving user details in Shared Preferences.
        if(typeCheck(mobile,password)) {
            ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
            Boolean isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {
                dialogLoginShow.setMessage("Logging in..");
                dialogLoginShow.show();
                new MyAsyncTask(this, makeRequestLogin(mobile, password), URLS.URL_LOGIN_VEHICLES_LIST, new MyAsyncTask.AsyncResponse() {
                    @Override
                    public void processFinish(String output) {
                        if (dialogLoginShow.isShowing()) {
                            dialogLoginShow.dismiss();
                            System.out.println("Output Received is : " + output);
                           if (decodeJsonUserLoginReceived(output) == 1) {
                                Intent intentLogIn = new Intent(MainActivity.this, VehiclesActivity.class);
                                intentLogIn.putExtra("Vehicles", output);
                                intentLogIn.putExtra("MobileNumber", mobile);
                                intentLogIn.putExtra("Password", password);
                                startActivity(intentLogIn);
                                finish();
                                System.exit(0);
                           } else
                                Toast.makeText(getApplicationContext(), "Invalid Credentials", Toast.LENGTH_SHORT).show();

                        }
                    }
                }).execute();
            } else {
                Toast.makeText(getApplicationContext(), "No Internet connection", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public String makeRequestLogin(String mob, String pass) {
        JSONObject request = new JSONObject();
        try
        {
            request.put("MobileNumber", mob);
            request.put("Password", pass);
        }
        catch(Exception e)
        {
            System.out.println("Exception encoding request..!!" + e);
        }
        return request.toString();
    }


    private int decodeJsonUserLoginReceived(String resultReceived){
        if(resultReceived!=null)
        {
            try{
                JSONObject response=new JSONObject(resultReceived);
                System.out.println(resultReceived);
               // Toast.makeText(getApplicationContext(), resultReceived , Toast.LENGTH_LONG).show();
                if(response.getInt("status")==1000)
                    return 1;
            }
            catch(Exception e)
            {
                System.out.println("Exception decoding vehicles list "+e);
            }
        }
        return 0;
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
    }

    public boolean typeCheck(String number, String password)
    {
                String msg="";
                boolean val=true;
        if(number.length()!=10) {
            msg = "Number Should be of 10 digits";
            val=false;
        }
        if(password.length()>12 &&password.length()<5) {
            msg = "Password should be of maximum 11 digits";
            val=false;
        }
        Toast.makeText(this,msg,Toast.LENGTH_SHORT).show();
        return val;
    }


}
