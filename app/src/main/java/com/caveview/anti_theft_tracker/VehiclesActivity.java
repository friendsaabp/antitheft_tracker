package com.caveview.anti_theft_tracker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.caveview.anti_theft_tracker.Services.GcmIntentService;
import com.caveview.anti_theft_tracker.adapter.VehicleSelectionAdapter;
import com.caveview.anti_theft_tracker.model.SessionManager;
import com.caveview.anti_theft_tracker.model.Vehicles;
import com.caveview.anti_theft_tracker.utils.ConnectionDetector;
import com.caveview.anti_theft_tracker.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;

public class VehiclesActivity extends AppCompatActivity {
    ArrayList<Vehicles> vehiclesList = new ArrayList<>();
    private ListView vehiclesListView;
    ProgressBar pb;
    private int selected_item = -1;
    SessionManager session;
    String MobileNumber;
    String Password;
    String Reg_Number;
    String ModelName;
    int mode;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicles);
        pb=(ProgressBar) findViewById(R.id.progressBar);
        vehiclesListView = (ListView) findViewById(R.id.vehicles_list);
        session = new SessionManager(getApplicationContext());

        Intent i= getIntent();
        String vehiclesResponse = i.getStringExtra("Vehicles");
        MobileNumber = i.getStringExtra("MobileNumber");
        Password = i.getStringExtra("Password");

       // Toast.makeText(getApplicationContext(), vehiclesResponse, Toast.LENGTH_LONG).show();
        vehiclesList = decodeVehiclesList(vehiclesResponse);
        System.out.println(vehiclesList);


      /*  mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Constants.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    String token = intent.getStringExtra("token");

                    Toast.makeText(getApplicationContext(), "GCM registration token: " + token, Toast.LENGTH_LONG).show();

                } else if (intent.getAction().equals(Constants.SENT_TOKEN_TO_SERVER)) {
                    // gcm registration id is stored in our server's MySQL

                    Toast.makeText(getApplicationContext(), "GCM registration token is stored in server!", Toast.LENGTH_LONG).show();

                } else if (intent.getAction().equals(Constants.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    Toast.makeText(getApplicationContext(), ""+intent.getStringExtra("message"), Toast.LENGTH_LONG).show();
                }

            }
        };
*/

        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if(isInternetPresent){
                    pb.setVisibility(View.GONE);
                    VehicleSelectionAdapter VSA = new VehicleSelectionAdapter(VehiclesActivity.this,  R.layout.activity_vehicles , vehiclesList);
                    vehiclesListView.setAdapter(VSA);
                    vehiclesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view1, int position, long id) {

                            Vehicles cat = (Vehicles) vehiclesListView.getItemAtPosition(position);
                            for (int i = 0; i < vehiclesListView.getChildCount(); i++) {
                                if (position == i) {
                                    vehiclesListView.getChildAt(i).setBackgroundColor(getResources().getColor(R.color.list_background_pressed));
                                    selected_item = position;
                                } else {
                                    vehiclesListView.getChildAt(i).setBackgroundColor(getResources().getColor(R.color.list_background));
                                }
                            }
                           // vehiclesListView.getChildAt(position).setBackgroundColor(Color.parseColor("#c8e8ff"));
                            String model = String.valueOf(cat.getVehicleModelName() );
                            String reg_no = String.valueOf(cat.getVehicleRegistrationNumber());
                          //  Toast.makeText(getApplicationContext(), "Model is: " + model + "and Registration Number is : " + reg_no, Toast.LENGTH_SHORT).show();
                        }
                    });
        }
        else
        {
            Toast.makeText(getApplicationContext(),"No Internet connection", Toast.LENGTH_LONG).show();
            pb.setVisibility(View.GONE);
        }

        Button b1 = (Button) findViewById(R.id.confirmVehicle);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vehiclePassInformation();
                // startActivity(new Intent(MainActivity.this, AndroidGPSTrackingActivity.class));
            }
        });

    }



    private ArrayList<Vehicles> decodeVehiclesList(String vehiclesReceived){
        ArrayList<Vehicles> vehiclesList = new ArrayList<>();

        try{
            JSONObject response=new JSONObject(vehiclesReceived);
            String data = response.getString("data");
            JSONArray vehiclesEncoded = new JSONArray(data);
            JSONObject mJsonObject = new JSONObject();
            for (int i = 0; i < vehiclesEncoded.length(); i++) {
                mJsonObject = vehiclesEncoded.getJSONObject(i);
                String vrn = mJsonObject.getString("RegistrationNumber");
                String vmn = mJsonObject.getString("ModelName");
                String mode=mJsonObject.getString("Mode");
                Vehicles v = new Vehicles(vmn, vrn,Integer.parseInt(mode));
                vehiclesList.add(v);
            }
        }
        catch(Exception e)
        {
            System.out.println("Exception decoding vehicles list "+e);
        }
        return vehiclesList;
    }


    private void vehiclePassInformation(){
        if(selected_item == -1){
            Toast.makeText(getApplicationContext(), "Please Select a Vehicle", Toast.LENGTH_LONG).show();
        }
        else{
            Intent gpsTrackingIntent = new Intent(VehiclesActivity.this, AndroidGPSTrackingActivity.class);
            Reg_Number = vehiclesList.get(selected_item).getVehicleRegistrationNumber();
            ModelName = vehiclesList.get(selected_item).getVehicleModelName();
            mode=vehiclesList.get(selected_item).getMode();
           // gpsTrackingIntent.putExtra("RegistrationNumber",Reg_Number );
           // gpsTrackingIntent.putExtra("ModelName", ModelName );
            session.createLoginSession(MobileNumber, Password, Reg_Number, ModelName);

           // registerGCM();

            SharedPreferences saved_values = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor=saved_values.edit();
            editor.putInt("Mode",mode);
            editor.putString("VEHICLE_REGISTRATION_NUMBER",Reg_Number);
            editor.commit();
            startActivity(gpsTrackingIntent);
            finish();
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        session.logoutUser();
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
    }

   /* @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Constants.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Constants.PUSH_NOTIFICATION));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }
*/
}
