package com.caveview.anti_theft_tracker.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by ASHISH on 5/29/2016.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "LocationDatabase";

    // Locations table name
    private static final String TABLE_LOCATION = "locationTable";

    // Table Columns names
    private static final String latitude = "latitude";
    private static final String longitude = "longitude";
    private static final String timestamp = "timestamp";
    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_LOCATION_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_LOCATION + "("
                + latitude + " TEXT," + longitude + " TEXT,"
                + timestamp + " TEXT PRIMARY KEY" + ")";
        db.execSQL(CREATE_LOCATION_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOCATION);


        onCreate(db);
    }



    public void inertLocation(LocationModel locationModel) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(latitude, locationModel.getLatitude()); // Latitude
        values.put(longitude, locationModel.getLongitude()); // Longitude
        values.put(timestamp, locationModel.getTimestamp()); // Longitude

        Log.v("com.caveview","Lat "+locationModel.getLatitude());
        Log.v("com.caveview","Long "+locationModel.getLongitude());
        Log.v("com.caveview","Time "+locationModel.getTimestamp());
        // Inserting Row
        db.insert(TABLE_LOCATION, null, values);
        db.close(); // Closing database connection
    }

    public ArrayList<LocationModel> getAllRecords() {
        ArrayList<LocationModel> locationModels=new ArrayList();
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_LOCATION, new String[] { latitude,
                        longitude, timestamp }, null,
                null, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            try {
                while (cursor.moveToNext()) {
                    LocationModel locationModel = new LocationModel(cursor.getString(0),
                            cursor.getString(1), cursor.getString(2));
                    locationModels.add(locationModel);

                }

            } finally {
                cursor.close();
            }


        }

        return locationModels;
    }

    public void deletePreviousData(String timestamp)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_LOCATION, timestamp + " <= ?",
                new String[] { timestamp });
        db.close();
    }

    @Override
    protected void finalize() throws Throwable {
        this.close();
        super.finalize();
    }

}
