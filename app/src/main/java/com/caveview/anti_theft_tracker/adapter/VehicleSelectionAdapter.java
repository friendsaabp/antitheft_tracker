package com.caveview.anti_theft_tracker.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.caveview.anti_theft_tracker.R;
import com.caveview.anti_theft_tracker.model.Vehicles;

import java.util.ArrayList;

/**
 * Created by Archit on 30-05-2016.
 */
public class VehicleSelectionAdapter extends ArrayAdapter {

    Context context;
    private ArrayList<Vehicles> vehicleList;
    private int mSelectedItem;
    private static LayoutInflater inflater=null;

    public VehicleSelectionAdapter(Context context, int textViewResourceId, ArrayList<Vehicles> vehicleList) {
        super(context, textViewResourceId, vehicleList);
        inflater = ( LayoutInflater )context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.vehicleList = vehicleList;
        //System.out.println("Category list is : " + categoryList);
    }

    @Override
    public int getCount() {
        return vehicleList.size();
    }

    @Override
    public Object getItem(int position)
    {
        return vehicleList.get(position);
    }

    public class Holder
    {
        TextView VehicleModelName;
        TextView VehicleRegistraionNumber;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder=new Holder();
        View rowView = inflater.inflate(R.layout.vehicle_item, null);
        try {

            holder.VehicleModelName = (TextView) rowView.findViewById(R.id.vehicle_model_name);
            holder.VehicleModelName.setText(vehicleList.get(position).getVehicleModelName() + "");

            holder.VehicleRegistraionNumber = (TextView) rowView.findViewById(R.id.vehicle_registration_num);
            holder.VehicleRegistraionNumber.setText(Html.fromHtml(vehicleList.get(position).getVehicleRegistrationNumber()+""));
        }
        catch (Exception e)
        {
            System.out.println("Exception Occured : "+e);
        }


        return rowView;
    }


}