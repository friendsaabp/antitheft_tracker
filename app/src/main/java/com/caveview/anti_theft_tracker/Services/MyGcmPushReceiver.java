package com.caveview.anti_theft_tracker.Services;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.caveview.anti_theft_tracker.AndroidGPSTrackingActivity;
import com.caveview.anti_theft_tracker.MainActivity;
import com.caveview.anti_theft_tracker.utils.Constants;
import com.caveview.anti_theft_tracker.utils.NotificationUtils;
import com.google.android.gms.gcm.GcmListenerService;


import org.json.JSONObject;


/**
 * Created by ASHISH on 5/8/2016.
 */
public class MyGcmPushReceiver extends GcmListenerService {
    private static final String TAG = MyGcmPushReceiver.class.getSimpleName();

    private NotificationUtils notificationUtils;




    @Override
    public void onMessageReceived(String from, Bundle bundle) {
        String title = bundle.getString("title");
        String text=bundle.getString("data");
        JSONObject data;
        String message="";
        int mode=0;
        try {
            data= new JSONObject(text);

            message= data.getString("message");
            mode=Integer.parseInt(data.getString("Mode"));

            SharedPreferences saved_values = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor=saved_values.edit();
            editor.putInt("Mode",mode);
            editor.commit();

           // AndroidGPSTrackingActivity.toggleActiveStealthMode(mode);

        }catch(Exception e)
        {

        }


        Log.e(TAG, "From: " + from);
        Log.e(TAG, "Title: " + title);
        Log.e(TAG,"Message :"+message);
        Log.v(TAG,"Mode:"+mode);


        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {

            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Constants.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            pushNotification.putExtra("Mode",mode);

            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
            System.out.println("App in Foreground");
            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils();
            notificationUtils.playNotificationSound();
        } else {

            Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
            resultIntent.putExtra("message", message);
            resultIntent.putExtra("Mode",mode);
            resultIntent.putExtra("Type",1);
            System.out.println("App in Background");

                showNotificationMessage(getApplicationContext(), title, message, "13515", resultIntent);


            }
        }


    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }


}