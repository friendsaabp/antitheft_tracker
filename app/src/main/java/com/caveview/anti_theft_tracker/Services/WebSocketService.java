package com.caveview.anti_theft_tracker.Services;

import android.app.IntentService;
import android.content.Intent;
import android.nfc.Tag;
import android.util.Log;

import com.caveview.anti_theft_tracker.utils.URLS;

import de.tavendo.autobahn.WebSocketConnection;
import de.tavendo.autobahn.WebSocketException;
import de.tavendo.autobahn.WebSocketHandler;

/**
 * Created by ASHISH on 5/31/2016.
 */
public class WebSocketService extends IntentService {

    private static final String TAG = "com.caveview";

    private final WebSocketConnection mConnection = new WebSocketConnection();


    public WebSocketService() {
        super("Web Socket Service");
    }

    private void start() {



        try {
            System.out.println("Trying to connect to "+URLS.URL_SEND_LOCATION_UPDATES);
            mConnection.connect(URLS.URL_SEND_LOCATION_UPDATES, new WebSocketHandler() {

                @Override
                public void onOpen() {
                    Log.d(TAG, "Status: Connected to " + URLS.URL_SEND_LOCATION_UPDATES);
                    mConnection.sendTextMessage("What the fuck are you doing ?");
                }

                @Override
                public void onTextMessage(String payload) {
                    Log.d(TAG, "Got echo: " + payload);
                }

                @Override
                public void onClose(int code, String reason) {
                    Log.d(TAG, "Connection lost.");
                }
            });
        } catch (WebSocketException e) {

            Log.d(TAG, e.toString());
        }
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        Log.v(TAG,"Service Started");
        start();
    }
}
