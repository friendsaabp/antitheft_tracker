package com.caveview.anti_theft_tracker.Services;

/**
 * Created by ASHISH on 5/8/2016.
 */
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;


import com.caveview.anti_theft_tracker.R;
import com.caveview.anti_theft_tracker.model.SessionManager;
import com.caveview.anti_theft_tracker.utils.ConnectionDetector;
import com.caveview.anti_theft_tracker.utils.Constants;
import com.caveview.anti_theft_tracker.utils.MyAsyncTask;
import com.caveview.anti_theft_tracker.utils.URLS;
import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import org.json.JSONObject;

import java.io.IOException;


public class GcmIntentService extends IntentService {

    private static final String TAG = "GCMINTENTSERVICE";
    SessionManager sessionManager;
    Context context;
    static String token;
    public static final String KEY = "key";
    public static final String TOPIC = "topic";
    public static final String SUBSCRIBE = "subscribe";
    public static final String UNSUBSCRIBE = "unsubscribe";

    public GcmIntentService() {
        super(TAG);
        context=this;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        sessionManager=new SessionManager(this);

        try {



            String key = intent.getStringExtra(KEY);
            switch (key) {
                case SUBSCRIBE:
                    // subscribe to a topic
                    String topic = intent.getStringExtra(TOPIC);
                    subscribeToTopic(topic);
                    break;
                default:
                    // if key is not specified, register with GCM
                    if(!sessionManager.getGcmTokenStatus()) {
                        InstanceID instanceID = InstanceID.getInstance(this);
                        token = instanceID.getToken(Constants.senderId,
                                GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);

                        Log.i(TAG, "GCM Registration Token: " + token);


                        sendRegistrationToServer(token);



                    }
            }

        } catch (Exception e) {
            Log.d(TAG, "Failed to complete token refresh", e);

            sessionManager.setGCMTokenStatus(false);
        }

    }




    private void sendRegistrationToServer(String token) {


        ConnectionDetector cd = new ConnectionDetector(this);
        Boolean isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {

            new MyAsyncTask(this, makeRequestForGcmUpdate(token), URLS.URL_TO_SET_GCM_TOKEN_FOR_TRACKER, new MyAsyncTask.AsyncResponse() {
                @Override
                public void processFinish(String output) {

                        System.out.println("Output Received is : " + output);

                        if(isSuccess(output)) {
                            sessionManager.setGCMTokenStatus(true);
                            Intent registrationComplete = new Intent(Constants.REGISTRATION_COMPLETE);
                            registrationComplete.putExtra("token", GcmIntentService.token);
                            LocalBroadcastManager.getInstance(context).sendBroadcast(registrationComplete);
                        }
                        else
                            Toast.makeText(context,"Failed to insert auth-token to server ",Toast.LENGTH_SHORT).show();

                        }



                    }

            ).execute();
        } else {
        }
    }


    public String makeRequestForGcmUpdate(String token)
    {
        JSONObject jsonObject=new JSONObject();
        try{
            jsonObject.put("gcm_id",token);
            jsonObject.put("registration_number",sessionManager.getRegNumber());
        }
        catch(Exception e)
        {
            System.out.println("Exception in sending gcm token "+e );
        }
        System.out.println(jsonObject.toString());
        return jsonObject.toString();
    }

    public boolean isSuccess(String out)
    {
        JSONObject jsonObject;
        try{
           jsonObject =new JSONObject(out);
            if(jsonObject.getString("error").contains("false"))
                return  true;
        }
        catch(Exception e)
        {
            System.out.println("Exception in sending gcm token "+e );
            return false;
        }
        return false;
    }


    public void subscribeToTopic(String topic) {
        GcmPubSub pubSub = GcmPubSub.getInstance(getApplicationContext());
        InstanceID instanceID = InstanceID.getInstance(getApplicationContext());
        String token = null;
        try {
            token = instanceID.getToken(Constants.senderId,
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            if (token != null) {
                pubSub.subscribe(token, "/topics/" + topic, null);
                Log.e(TAG, "Subscribed to topic: " + topic);
            } else {
                Log.e(TAG, "error: gcm registration id is null");
            }
        } catch (IOException e) {
            Log.e(TAG, "Topic subscribe error. Topic: " + topic + ", error: " + e.getMessage());
            Toast.makeText(getApplicationContext(), "Topic subscribe error. Topic: " + topic + ", error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void unsubscribeFromTopic(String topic) {
        GcmPubSub pubSub = GcmPubSub.getInstance(getApplicationContext());
        InstanceID instanceID = InstanceID.getInstance(getApplicationContext());
        String token = null;
        try {
            token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
            if (token != null) {
                pubSub.unsubscribe(token, "");
                Log.e(TAG, "Unsubscribed from topic: " + topic);
            } else {
                Log.e(TAG, "error: gcm registration id is null");
            }
        } catch (IOException e) {
            Log.e(TAG, "Topic unsubscribe error. Topic: " + topic + ", error: " + e.getMessage());
            Toast.makeText(getApplicationContext(), "Topic subscribe error. Topic: " + topic + ", error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


}
