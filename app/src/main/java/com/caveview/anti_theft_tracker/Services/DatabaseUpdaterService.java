package com.caveview.anti_theft_tracker.Services;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.caveview.anti_theft_tracker.Database.DatabaseHandler;
import com.caveview.anti_theft_tracker.Database.LocationModel;

import com.caveview.anti_theft_tracker.utils.ConnectionDetector;
import com.caveview.anti_theft_tracker.utils.URLS;
import com.caveview.anti_theft_tracker.utils.WebService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by ASHISH on 6/1/2016.
 */
public class DatabaseUpdaterService extends IntentService {

    DatabaseHandler databaseHandler;
    private String TAG="com.caveview.DatabaseUpdaterService";
    private String VEHICLE_REGISTRATION_NUMBER="REG_NUMBER";
    private String LATITUDE="LATITUDE";
    private String LONGITUDE="LONGITUDE";
    private String TIMESTAMP="TIMESTAMP";
    private String LOCATIONS_ARRAY="LOCATIONS_ARRAY";
    SharedPreferences sessionManager ;
    String lastTimestamp;
    public DatabaseUpdaterService()
    {
        super("Database Updater Service");
        databaseHandler=new DatabaseHandler(this);

    }
    @Override
    protected void onHandleIntent(Intent intent) {

       sessionManager= PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        ArrayList<LocationModel> locationModels = databaseHandler.getAllRecords();
        int numRecords = locationModels.size();
        if (numRecords > 0) {
            JSONObject data = getPostRequestData(locationModels);
            ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
            Boolean isInternetPresent = cd.isConnectingToInternet();
            int mode = sessionManager.getInt("Mode",1);
           if(isInternetPresent && mode==1)
           {
               try{
                   System.out.println("Data sent "+data);
                   String response = new WebService(URLS.URL_SEND_LOCATION_UPDATES).executePost(data.toString());

                   System.out.println("Response in Async Task " + response);
                   if(checkValidResponse(response))
                       databaseHandler.deletePreviousData(lastTimestamp);



               }
               catch(Exception e)
               {
                   System.out.println("Exception is " + e);
               }
           }
            else if(mode==0)
           {
               stopSelf();
           }

        }
    }
    public JSONObject getPostRequestData(ArrayList<LocationModel> models)
    {
        JSONObject jsonObject=new JSONObject();
        try{
            String v_reg_number=sessionManager.getString("VEHICLE_REGISTRATION_NUMBER","");
            jsonObject.put(VEHICLE_REGISTRATION_NUMBER,v_reg_number);
            JSONArray locations=new JSONArray();
            int numRecords=models.size();
            for(int i=0;i<numRecords;i++)
            {
                JSONObject jsonObject1=new JSONObject();
                LocationModel locationModel=models.get(i);
                jsonObject1.put(LATITUDE,locationModel.getLatitude());
                jsonObject1.put(LONGITUDE,locationModel.getLongitude());
                jsonObject1.put(TIMESTAMP,locationModel.getTimestamp());
                locations.put(jsonObject1);
                if(i==numRecords-1)
                    lastTimestamp=locationModel.getTimestamp();
            }
            jsonObject.put(LOCATIONS_ARRAY,locations);
        }catch(Exception e)
        {
            Log.v(TAG,"Exception in encoding post request "+e);
        }
        return jsonObject;
    }


    public Boolean checkValidResponse(String response)
    {
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt("status") == 1000)
                return true;
        }catch(Exception e)
        {
            Log.v(TAG,"Exception in decoding post request");
        }
        return false;
    }
}
