package com.caveview.anti_theft_tracker.Services;


import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;

import android.content.IntentSender;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;

import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.caveview.anti_theft_tracker.Database.DatabaseHandler;
import com.caveview.anti_theft_tracker.Database.LocationModel;
import com.caveview.anti_theft_tracker.MainActivity;
import com.caveview.anti_theft_tracker.model.SessionManager;
import com.caveview.anti_theft_tracker.utils.MyAsyncTask;
import com.caveview.anti_theft_tracker.utils.URLS;
import com.caveview.anti_theft_tracker.utils.WebService;
import com.google.android.gms.common.ConnectionResult;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONObject;

/**
 * Created by ASHISH on 5/28/2016.
 */
public class LocationTrackerService extends IntentService implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {


    private Location mLastLocation;

    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;

    // boolean flag to toggle periodic location updates
    private boolean mRequestingLocationUpdates = false;

    private LocationRequest mLocationRequest;

    // Location updates intervals in sec
    private static int UPDATE_INTERVAL = 5000; // 10 sec
    private static int FATEST_INTERVAL = 500; // 5 sec
    private static int DISPLACEMENT = 10; // 10 meters

    private String TAG="com.caveview";


    double prevLat=0d;
    double prevLong=0d;

    long prevTimestamp=0;

    int mode;



    private  SharedPreferences sessionManager;



    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public LocationTrackerService(String name) {
        super(name);
    }

    public LocationTrackerService(){super("Location Tracker Service");}



    @Override
    protected void onHandleIntent(Intent intent) {
        mLocationRequest = LocationRequest.create().setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FATEST_INTERVAL)
                .setSmallestDisplacement(DISPLACEMENT);

        sessionManager= PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext());
        mode=sessionManager.getInt("Mode",1);


            buildGoogleApiClient();
            if (mGoogleApiClient != null) {
                mGoogleApiClient.connect();

            }




    }

    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest,this);
    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    private void displayLocation() {
        try{

            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }catch (Exception e)
        {
            e.printStackTrace();
        }

        if (mLastLocation != null) {
            double latitude = mLastLocation.getLatitude();
            double longitude = mLastLocation.getLongitude();
            if(hasLocationChanged(latitude,longitude)) {
                //Toast.makeText(this, " ( " + latitude + " , " + longitude + " )", Toast.LENGTH_SHORT).show();
                long timestamp = System.currentTimeMillis() / 1000;
                mode=sessionManager.getInt("Mode",1);
                Log.v(TAG,"Mode "+ mode);
                if(mode==1) {
                    if (timestamp - prevTimestamp > 15) {
                        new DatabaseHandler(this).inertLocation(new LocationModel(latitude + "", longitude + "", timestamp + ""));
                        //System.out.println("Data is " + new DatabaseHandler(this).getAllRecords().get(0).getTimestamp());
                        prevTimestamp = timestamp;
                    }
                }else
                {
                    Log.v(TAG,"Stealth Mode Applicable");
                        if(prevLat!=0d || prevLong !=0d) {
                            float dist=distFrom(prevLat, prevLong, latitude, longitude);
                           // Toast.makeText(this,"Dist is "+dist,Toast.LENGTH_SHORT).show();
                            if ( dist> 20) {

                                try {

                                    String reg_number = sessionManager.getString("VEHICLE_REGISTRATION_NUMBER", "default_reg_number");
                                    JSONObject request = new JSONObject();
                                    request.put("registration_number", reg_number);
                                    new MyAsyncTask(this, request.toString(), URLS.URL_TO_ALERT_INTRUSION, new MyAsyncTask.AsyncResponse() {
                                        @Override
                                        public void processFinish(String output) {
                                            System.out.println("Response in Location Tracker Alert Mode " + output);
                                        }
                                    }).execute();

                                } catch (Exception e) {
                                    System.out.println(TAG + " Exception in stealth mode locator service is " + e);
                                }
                            }
                        }



                }
                prevLat = latitude;
                prevLong = longitude;
            }


            startLocationUpdates();
        } else {
            Toast.makeText(this,"(Couldn't get the location. Make sure location is enabled on the device)",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i("Log 1", "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    @Override
    public void onConnected(Bundle arg0) {
        // Once connected with google api, get the location
        displayLocation();
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();
    }


    @Override
    public void onLocationChanged(Location location) {

        mLastLocation = location;

       /* Toast.makeText(getApplicationContext(), "Location changed!",
                Toast.LENGTH_SHORT).show();*/





        displayLocation();


    }

    private boolean hasLocationChanged(double lat,double Long)
    {

            if (prevLat != lat || prevLong != Long) {

                return true;
            }



        return false;
    }

    public static float distFrom(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        float dist = (float) (earthRadius * c);

        return dist;
    }



}
