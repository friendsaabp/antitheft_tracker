package com.caveview.anti_theft_tracker;


import com.caveview.anti_theft_tracker.Recievers.LocationAlarmReciever;
import com.caveview.anti_theft_tracker.Recievers.UpdaterAlarmReciever;
import com.caveview.anti_theft_tracker.Services.GcmIntentService;
import com.caveview.anti_theft_tracker.model.SessionManager;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.caveview.anti_theft_tracker.utils.Constants;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

public class AndroidGPSTrackingActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private final static int REQUEST_LOCATION = 0;


    SessionManager session;
    private PendingIntent pendingLocationIntent;
    private PendingIntent pendingUpdaterIntent;
    private  ImageView imageView1;
    private  ImageView imageView2;
    private boolean inhibit = true;
    private Button logout;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    int mode=0;
    private GoogleApiClient mGoogleApiClient;
   final int REQUEST_CHECK_SETTINGS=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_android_gpstracking);
        imageView1 = (ImageView) findViewById(R.id.imageActive);
        imageView2 = (ImageView) findViewById(R.id.imageStealth);
        logout= (Button) findViewById(R.id.logoutButton);
        session = new SessionManager(getApplicationContext());


        try {
            buildGoogleApiClient();
            if (mGoogleApiClient != null) {
                mGoogleApiClient.connect();


                settingsrequest();
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }


        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Constants.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    String token = intent.getStringExtra("token");

                   // Toast.makeText(getApplicationContext(), "GCM registration token: " + token, Toast.LENGTH_LONG).show();

                    session.setGCMTokenStatus(true);

                } else if (intent.getAction().equals(Constants.SENT_TOKEN_TO_SERVER)) {
                    // gcm registration id is stored in our server's MySQL

                 //   Toast.makeText(getApplicationContext(), "GCM registration token is stored in server!", Toast.LENGTH_LONG).show();

                } else if (intent.getAction().equals(Constants.PUSH_NOTIFICATION)) {
                    // new push notification is received

                   // Toast.makeText(getApplicationContext(), ""+intent.getStringExtra("message"), Toast.LENGTH_LONG).show();
                    mode=intent.getIntExtra("Mode",1);
                    if (mode == 1) {
                        imageView1.setImageResource(R.drawable.mapmarker3);
                        imageView2.setImageResource(android.R.color.transparent);
                    } else {
                        imageView2.setImageResource(R.drawable.mapmarker3);
                        imageView1.setImageResource(android.R.color.transparent);
                    }

                }

            }
        };

        if(session.getGcmTokenStatus()==false)
        registerGCM();





        Intent i = getIntent();
        mode = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext()).getInt("Mode", 1);
        if (mode == 1) {
            imageView1.setImageResource(R.drawable.mapmarker3);
            imageView2.setImageResource(android.R.color.transparent);
        } else {
            imageView2.setImageResource(R.drawable.mapmarker3);
            imageView1.setImageResource(android.R.color.transparent);
        }

        final String vehicleRegNumber = session.getRegNumber();
        final String vehicleModelName = session.getModelName();

       // Toast.makeText(getApplicationContext(), vehicleModelName + "  " + vehicleRegNumber, Toast.LENGTH_LONG).show();


        if(checkPlayServices()) {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]
                        {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
            }
            Intent locationAlarmIntent = new Intent(this, LocationAlarmReciever.class);
            pendingLocationIntent = PendingIntent.getBroadcast(this, 0, locationAlarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            final Intent updateAlarmIntent = new Intent(this, UpdaterAlarmReciever.class);
            pendingUpdaterIntent = PendingIntent.getBroadcast(this, 0, updateAlarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            final AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            int locationUpdateInterval = 2000;
            int databaseUdpadeInterval=72000;

            manager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), locationUpdateInterval, pendingLocationIntent);
            //Toast.makeText(this, "Location Alarm Set", Toast.LENGTH_SHORT).show();
            manager.setRepeating(AlarmManager.RTC_WAKEUP,System.currentTimeMillis(),databaseUdpadeInterval,pendingUpdaterIntent);
            //Toast.makeText(this, "Update Alarm Set", Toast.LENGTH_SHORT).show();


            logout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    session.logoutUser();
                    manager.cancel(pendingLocationIntent);
                    manager.cancel(pendingUpdaterIntent);

                    pendingLocationIntent.cancel();
                    pendingUpdaterIntent.cancel();

                }
            });



        }

    }


    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, (Activity)this.getApplicationContext(), PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }
            else {
                Toast.makeText(getApplicationContext(), "This device is not supported.", Toast.LENGTH_LONG).show();
                finish();
            }
            return false;
        }
        return true;
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    @Override
    public void onBackPressed() {

        System.out.println(inhibit);
        if (inhibit) {
            Toast.makeText(this, "Press again to exit", Toast.LENGTH_SHORT).show();
            inhibit = false;
            System.out.println(inhibit);
        } else {

            finish();
            System.out.println(inhibit);
        }
        System.out.println(inhibit);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }




    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Constants.REGISTRATION_COMPLETE));

        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Constants.PUSH_NOTIFICATION));


    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    private void registerGCM() {
        Intent intent = new Intent(this, GcmIntentService.class);
        intent.putExtra("key", "register");
        startService(intent);
    }


    public void settingsrequest()
    {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(AndroidGPSTrackingActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
// Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        //startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        settingsrequest();//keep asking if imp or do whatever
                        break;
                }
                break;
        }
    }


    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}
