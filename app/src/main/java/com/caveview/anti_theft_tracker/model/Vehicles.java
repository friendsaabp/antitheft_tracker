package com.caveview.anti_theft_tracker.model;

/**
 * Created by Archit on 30-05-2016.
 */
public class Vehicles {
    private String VehicleModel;
    private String VehicleRegistraionNumber;
    private int mode;

    public Vehicles(String vm, String vrn,int mode)
    {
        this.VehicleModel=vm;
        this.VehicleRegistraionNumber=vrn;
        this.mode=mode;
    }

     public String getVehicleModelName()
    {
        return this.VehicleModel;
    }

     public String getVehicleRegistrationNumber()
    {
        return this.VehicleRegistraionNumber;
    }

    public int getMode() {
        return mode;
    }
}
