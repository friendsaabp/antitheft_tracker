package com.caveview.anti_theft_tracker.model;

/**
 * Created by Archit on 2/3/2016.
 */
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;

import com.caveview.anti_theft_tracker.AndroidGPSTrackingActivity;
import com.caveview.anti_theft_tracker.MainActivity;

import java.util.HashMap;
import java.util.List;

public class SessionManager {
    // Shared Preferences
    SharedPreferences pref;
    SharedPreferences defaultSharedPreferences;
    // Editor for Shared preferences
    Editor editor,defaultSharedPreferencesEditor;
    // Context
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;
    // Sharedpref file name
    private static final String PREF_NAME = "UserPref";
    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";
    public static final String KEY_PHONE = "phoneKey";
    public static final String KEY_PASSWORD = "passwordKey";
    public static final String KEY_REG_NUMBER = "vehicleRegKey";
    public static final String KEY_MODEL_NAME = "modelNameKey";
    public static final String MODE="mode";
    public static final String KEY_TOKEN_STATUS="gcmtokenstatus";

    // Constructor
    public SessionManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        defaultSharedPreferences=PreferenceManager.getDefaultSharedPreferences(_context);
        defaultSharedPreferencesEditor=defaultSharedPreferences.edit();
        editor = pref.edit();
    }

    /**
     * Create login session
     * */
    public void createLoginSession(String phone, String password, String vehicleRegNumber, String modelName){
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);
        // Storing user values in pref
        editor.putString(KEY_PHONE, phone);
        editor.putString(KEY_REG_NUMBER, vehicleRegNumber);
        editor.putString(KEY_MODEL_NAME, modelName);
        defaultSharedPreferencesEditor.putInt("Allowed",1);
        defaultSharedPreferencesEditor.commit();
        // commit changes
        editor.commit();
    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     * */
    public void checkLogin(){
        // Check login status
        if(!this.isLoggedIn()){
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, AndroidGPSTrackingActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            // Staring Login Activity
            _context.startActivity(i);
        }
    }



    public int getMode(){
        return pref.getInt(MODE,0);
    }
    public void setMode(int mode){
        editor.putInt(MODE,mode);
        editor.commit();
    }


    /**
     * Clear session details
     * */
    public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();
        // After logout redirect user to Loging Activity
        Intent i = new Intent(_context, MainActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


        Editor editor=pref.edit();
        editor.clear();
        editor.commit();

        defaultSharedPreferencesEditor.putInt("Allowed",0);
        defaultSharedPreferencesEditor.commit();
        // Staring Login Activity
        _context.startActivity(i);
    }

    /**
     * Quick check for login
     * **/
    // Get Login State
    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

    public String getPhone(){
        return pref.getString(KEY_PHONE,"Phone Number");
    }

    public String getPassword(){
       return pref.getString(KEY_PASSWORD, "Password");
    }

    public String getRegNumber(){
        return pref.getString(KEY_REG_NUMBER, "Reg_Number");
    }

    public String getModelName()
    {
        return pref.getString(KEY_MODEL_NAME, "Model_Name");
    }

    public void setGCMTokenStatus(boolean val)
    {
        editor.putBoolean(KEY_TOKEN_STATUS,val);
        editor.commit();
    }

    public boolean getGcmTokenStatus()
    {
        return pref.getBoolean(KEY_TOKEN_STATUS,false);
    }

}