package com.caveview.anti_theft_tracker.utils;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.widget.Toast;

/**
 * Created by Archit on 08-05-2016.
 */
public class BatteryInformation {


    public void getBatteryStatus(Context context){
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);

        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);

        System.out.println(level);

        Toast.makeText(context, "Battery is : " + level + "%" , Toast.LENGTH_SHORT).show();
    }

    public void getChargingStatus(Context context){
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);
        int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);

        boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING || status == BatteryManager.BATTERY_STATUS_FULL;
        System.out.println("CHARGING??" + isCharging);


        int chargePlug = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
        boolean acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;

        System.out.println("USB PLUGGED IN ??" + usbCharge);
        System.out.println("AC PLUGGED IN ??" + acCharge);

        Toast.makeText(context, "Charging using USB " + usbCharge , Toast.LENGTH_SHORT).show();

        Toast.makeText(context, "Charging using AC" + acCharge , Toast.LENGTH_SHORT).show();
    }

}
