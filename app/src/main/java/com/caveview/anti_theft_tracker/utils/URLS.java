package com.caveview.anti_theft_tracker.utils;

public class URLS {
    public static String URL_LOGIN_VEHICLES_LIST = "http://trichashapps.com/college_project/login_and_getTrackerVehicles.php";
   // public static String URL_SEND_LOCATION_UPDATES="ws://192.168.1.79:8080/echobot";
    public static String URL_SEND_LOCATION_UPDATES="http://trichashapps.com/college_project/sendLocationUpdates.php";

    public static String URL_TO_SET_GCM_TOKEN_FOR_TRACKER="http://trichashapps.com/college_project/v1/tracker/setGcm";
    public static String URL_TO_ALERT_INTRUSION="http://trichashapps.com/college_project/v1/tracker/alertUser";

    }
