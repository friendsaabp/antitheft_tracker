package com.caveview.anti_theft_tracker.utils;

import android.content.Context;
import android.content.Intent;

/**
 * Created by ASHISH on 5/8/2016.
 */
public class Constants {

    public static String senderId="223274504376";


    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // type of push messages
    public static final int PUSH_TYPE_CHATROOM = 1;
    public static final int PUSH_TYPE_USER = 2;

    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    static final String DISPLAY_MESSAGE_ACTION =
            "com.caveview.anti_theft_tracker.DISPLAY_MESSAGE";


    // id to handle the notification in the notification try
    public static final int NOTIFICATION_ID = 100;

    static final String EXTRA_MESSAGE = "message";

    static void displayMessage(Context context, String message) {
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        intent.putExtra(EXTRA_MESSAGE, message);
        context.sendBroadcast(intent);
    }

}
